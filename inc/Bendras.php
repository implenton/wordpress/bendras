<?php

namespace Bendras;

class Bendras {

    protected $modules;
    protected $activeModules;
    protected $moduleFiles = [
        'acf:areng'           => 'areng/advanced-custom-fields',
        'cleanup:areng'       => 'areng/cleanup',
        'gf:areng'            => 'areng/gravity-forms',
        'guard-plugins:areng' => 'areng/guard-plugins',
        'leap-to:areng'       => 'areng/leap-to',
        'link:areng'          => 'areng/link',
        'navigation:areng'    => 'areng/navigation',
        'part:areng'          => 'areng/part',
        'redirect:areng'      => 'areng/redirect',
        'id:areng'            => 'areng/id',
        'setup-theme:areng'   => 'areng/setup-theme',
        'svg:areng'           => 'areng/svg',
        'typekit:areng'       => 'areng/typekit',
        'wrap:areng'          => 'areng/wrap',
    ];

    public function __construct( $modules = [] ) {

        $this->modules = $modules;

        if ( is_array( $this->modules ) ) {
            $this->init();
        }

    }

    protected function init() {

        $this->activeModules = $this->matchModulesToFiles( $this->modules[0] );
        $this->activate( $this->activeModules );

    }

    protected function matchModulesToFiles( $modules ) {

        $modulesFiles = [];

        foreach ( $modules as $module ) {
            $modulesFiles[] = $this->moduleFiles[ $module ];
        }

        return $modulesFiles;

    }

    protected function activate( $modules ) {

        foreach ( $modules as $module ) {
            require plugin_dir_path( __FILE__ ) . "{$module}.php";
        }

    }

}