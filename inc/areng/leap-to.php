<?php

namespace Bendras\LeapTo;

/**
 * Returns the escaped version of the home url with a trailing slash at the end.
 */
function get_home() {

    return esc_url( \home_url( '/' ) );

}

/**
 * Echoes the escaped version of the home url with a trailing slash at the end.
 */
function the_home() {

    echo get_home();

}

/**
 * Returns the url to posts page. If it is not set, it returns the home url.
 */
function get_blog() {

    if ( $page_for_posts = \get_option( 'page_for_posts' ) ) {
        return get_permalink( $page_for_posts );
    }

    return get_home();

}

/**
 * Echoes the url to posts page. If it is not set, it returns the home url.
 */
function the_blog() {

    echo get_blog();

}

/**
 * Returns the url to front page. If it is not set, it returns the home url.
 */
function get_front_page() {

    if ( $page_on_front = \get_option( 'page_on_front' ) ) {
        return get_permalink( $page_on_front );
    }

    return get_home();

}

/**
 * Echoes the url to front page. If it is not set, it returns the home url.
 */
function the_front_page() {

    echo get_front_page();

}

function get_woo_account() {

    if ( $woocommerce_myaccount_page_id = \get_option( 'woocommerce_myaccount_page_id' ) ) {
        return get_permalink( $woocommerce_myaccount_page_id );
    }

    return get_home();

}

function the_woo_account() {

    echo get_woo_account();

}

function get_woo_cart() {

    if ( $woocommerce_cart_page_id = \get_option( 'woocommerce_cart_page_id' ) ) {
        return get_permalink( $woocommerce_cart_page_id );
    }

    return get_home();

}

function the_woo_cart() {

    echo get_woo_cart();

}

function get_archive( $post_type ) {

    return get_post_type_archive_link( $post_type );

}

function the_archive( $post_type ) {

    echo get_archive( $post_type );

}

function the_template( $name, $post_type = 'page', $dir = 'templates' ) {

    echo get_template( $name, $post_type, $dir );

}

function get_template( $name, $post_type = 'page', $dir = 'templates' ) {

    if ( ! $id = \Bendras\ID\get_template( $name, $post_type, $dir ) ) {

        return false;

    }

    return get_permalink( $id );

}