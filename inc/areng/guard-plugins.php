<?php

namespace Bendras\GuardPlugins;

function disable_deactivate_edit( $actions, $plugin_file ) {

    $protected_plugins = [
        'advanced-custom-fields-pro/acf.php',
        'advanced-custom-fields-table-field/acf-table.php',
        'bendras/bendras.php',
        'github-implenton-bendras/bendras.php',
        'github-implenton-jaotama/jaotama.php',
        'github-implenton-tinklas/tinklas.php',
        'github-updater/github-updater.php',
        'gravityforms/gravityforms.php',
        'jaotama/jaotama.php',
        'safe-svg/safe-svg.php',
        'tinklas/tinklas.php',
    ];

    $protected_plugins = apply_filters( 'bendras_guarded_plugins', $protected_plugins );

    if ( in_array( $plugin_file, $protected_plugins ) ) {

        unset( $actions['deactivate'] );
        unset( $actions['edit'] );

    }

    return $actions;

}

add_filter( 'plugin_action_links', __NAMESPACE__ . '\\disable_deactivate_edit', 10, 2 );