<?php

namespace Bendras;

class GravityForms {

    protected $form_id;
    protected $form;

    public function __construct( $form_id ) {

        $this->form_id = $form_id;
        $this->form    = $this->get_form( $form_id );

    }

    protected function get_form( $form_id ) {

        return \GFAPI::get_form( $form_id );

    }

    public function title() {

        return $this->form['title'];

    }

    public function description() {

        return $this->form['description'];

    }

    public function display(
        $display_title = false,
        $display_description = false,
        $display_inactive = false,
        $field_values = null,
        $ajax = true,
        $tabindex = '',
        $echo = false
    ) {

        return gravity_form( $this->form_id, $display_title, $display_description, $display_inactive, $field_values,
            $ajax, $tabindex, $echo );

    }

}