<?php

namespace Bendras\SetupTheme;

add_action( 'after_setup_theme', __NAMESPACE__ . '\\handle', 30 );

function handle() {

    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );

    add_theme_support( 'html5', [
        'comment-list',
        'comment-form',
        'search-form',
        'gallery',
        'caption',
    ] );

    add_theme_support( 'woocommerce' );

    register_nav_menus( [
        'social' => __( 'Social Networks', 'bendras' ),
    ] );

}

define( 'ICL_DONT_LOAD_NAVIGATION_CSS', true );
define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );
define( 'ICL_DONT_LOAD_LANGUAGES_JS', true );