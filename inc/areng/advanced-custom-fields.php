<?php

namespace Bendras\ACF;

function get_image_or_svg( $image_arr, $size = 'medium' ) {

    if ( strpos( $image_arr['mime_type'], 'svg' ) === false ) {
        return wp_get_attachment_image( $image_arr['id'], $size );
    }

    return \Bendras\SVG\get_from_url( $image_arr['url'] );

}

function the_image_or_svg( $image_arr, $size = 'medium' ) {

    echo get_image_or_svg( $image_arr, $size );

}

/**
 * Match flexible content layouts to partial files.
 *
 * The default directory for partial files is flexible-layouts. File names should be delimited with a dashes, not with
 * and underscores.
 *
 * @param array  $field_value Value of the flexible field.
 * @param string $dir         Name of directory in which the partial files resides.
 */
function load_flexible_files( $field_value, $dir = 'flexible-layouts' ) {

    if ( empty( $field_value ) ) {
        return false;
    }

    if ( ! empty( $dir ) ) {
        $dir = trailingslashit( $dir );
    }

    foreach ( $field_value as $fields ) :

        $file = str_replace( '_', '-', $fields['acf_fc_layout'] );

        \Bendras\Part\load( $dir . $file, compact( 'fields' ) );

    endforeach;

}