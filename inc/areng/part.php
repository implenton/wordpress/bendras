<?php

namespace Bendras\Part;

function load( $file, $args = [] ) {

    $located = find( $file );

    if ( ! file_exists( $located ) ) {
        return false;
    }

    include_part( $located, $args );

}

function load_many( $files, $dir = '', $args = [] ) {

    if ( ! empty( $dir ) ) {
        $dir = trailingslashit( $dir );
    }

    foreach ( $files as $file ) {
        load( $dir . $file, $args );
    }

}

function find( $file ) {

    $template_parts_dir = \apply_filters( 'bendras_template_parts_dir', 'template-parts' );

    if ( ! empty( $template_parts_dir ) ) {
        $template_parts_dir = trailingslashit( $template_parts_dir );
    }

    return locate_template( "{$template_parts_dir}{$file}.php" );

}

function include_part( $file, $args ) {

    global $wp_query;

    $wp_query->template_part = $args;
    extract( $wp_query->template_part );

    // TODO: find a more elegant solution to get the name of file included
    $file_path_to_remove = trailingslashit( get_stylesheet_directory() );
    $file_path_to_remove .= trailingslashit( apply_filters( 'bendras_template_parts_dir', 'template-parts' ) );
    $file_included       = str_replace( $file_path_to_remove, '', $file );
    $file_included       = str_replace( '.php', '', $file_included );

    do_action( "bendras_before_{$file_included}_included", $args );

    include( $file );

    do_action( "bendras_after_{$file_included}_included", $args );

    unset( $wp_query->template_part );

}