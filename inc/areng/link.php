<?php

namespace Bendras\Link;

function the_title() {

    $link  = get_permalink();
    $title = get_the_title();

    printf( '<a href="%1$s">%2$s</a>', $link, $title );

}

function the_image( $size = 'thumbnail', $image_id = '', $args = '' ) {

    if ( empty( $image_id ) ) {
        $image_id = get_post_thumbnail_id();
    }

    $image_html = wp_get_attachment_image( $image_id, $size, null, $args );
    $link       = get_permalink();

    printf( '<a href="%1$s">%2$s</a>', $link, $image_html );

}

function the_more( $text = 'More…' ) {

    $link = get_permalink();

    printf( '<a href="%1$s">%2$s</a>', $link, $text );

}