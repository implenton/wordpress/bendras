<?php

namespace Bendras\Typekit;

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\init' );

function init() {

    $kitCode = apply_filters( 'bendras_typekit_code', '' );

    if ( ! empty( $kitCode ) ) {

        wp_register_script( 'typekit', "https://use.typekit.net/{$kitCode}.js" );
        wp_add_inline_script( 'typekit', 'try { Typekit.load({async: true}); } catch (e) {}' );

        wp_enqueue_script( 'typekit' );

    }

}