<?php

namespace Bendras\SVG;

function get_local( $file ) {

    $svgs_dir = apply_filters( 'bendras_svgs_dir', 'assets/svg' );

    if ( ! empty( $svgs_dir ) ) {
        $svgs_dir = trailingslashit( $svgs_dir );
    }

    $file = get_theme_file_path( "{$svgs_dir}{$file}.svg" );

    if ( ! file_exists( $file ) ) {
        return false;
    }

    return _clean( file_get_contents( $file ) );

}

function display_local( $file ) {

    echo get_local( $file );

}

function get_from_url( $file ) {

    return _clean( file_get_contents( $file ) );

}

function display_from_url( $file ) {

    echo get_from_url( $file );

}

function _clean( $content ) {

    $content = trim( str_replace( PHP_EOL, '', $content ) );
    $content = preg_replace( '/(\>)\s*(\<)/m', '$1$2', $content );

    return $content;

}