<?php

namespace Bendras\Redirect;

function to_first_child( $post_id = '', $get_pages_args = [] ) {

    global $post;

    if ( empty( $post_id ) ) {
        $post_id = $post->ID;
    }

    $args = wp_parse_args( $get_pages_args, [
        'parent'      => $post_id,
        'sort_column' => 'menu_order',
        'number'      => 1,
    ] );

    if ( $first_child = get_pages( $args ) ) {
        wp_redirect( get_permalink( $first_child[0]->ID ) );
    }

}