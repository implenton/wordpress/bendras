<?php

namespace Bendras\Navigation;

/**
 * wp_nav_menu prepended with a tag with the name of the menu.
 *
 * @param string $location         The theme location.
 * @param array  $nav_menu_args    Any arguments that you would pass to the wp_nav_menu.
 * @param string $menu_title_tag   The tag in which the menu title is wrapped. Defaults to div.
 * @param string $menu_title_class Class in which the name of the menu is wrapper. Defaults to menu-title.
 */
function menu_with_title( $location, $nav_menu_args = [], $menu_title_tag = 'div', $menu_title_class = 'menu-title' ) {

    $menu_locations = get_nav_menu_locations();
    $menu           = get_term( $menu_locations[ $location ], 'nav_menu' );

    if ( is_wp_error( $menu ) ) {
        return false;
    }

    $args = wp_parse_args( $nav_menu_args, [
        'theme_location' => $location,
        'container'      => '',
    ] );

    printf( '<%1$s class="%2$s">%3$s</%1$s>', $menu_title_tag, $menu_title_class, $menu->name );

    wp_nav_menu( $args );

}

function the_sub_pages_list( $include_parent = true ) {

    echo get_sub_pages_list( $include_parent );

}

function get_sub_pages_list( $include_parent = true ) {

    $queried_object = get_queried_object();
    $child_of       = $queried_object->ID;

    if ( $queried_object->post_parent != 0 ) {

        $child_of = $queried_object->post_parent;

    }

    $html = '';

    if ( $include_parent ) {

        $html .= wp_list_pages( [
            'title_li'    => '',
            'include'     => $child_of,
            'echo'        => false,
            'sort_column' => 'menu_order',
        ] );

    }

    $html .= wp_list_pages( [
        'title_li'    => '',
        'child_of'    => $child_of,
        'echo'        => false,
        'sort_column' => 'menu_order',
    ] );

    return sprintf( '<ul>%1$s</ul>', $html );

}

function the_terms_list( $taxonomy = 'category', $highlight_on_single = true, $list_args = '' ) {

    $args = [
        'taxonomy' => $taxonomy,
        'title_li' => '',
        'echo'     => false,
    ];

    if ( is_single() && $highlight_on_single ) {

        $current_terms = wp_get_post_terms( get_queried_object_id(), $taxonomy, [
            'fields' => 'ids',
        ] );

        $args = wp_parse_args( [
            'current_category' => $current_terms,
        ], $args );

    }

    if ( ! empty( $list_args ) ) {
        $args = wp_parse_args( $list_args, $args );
    }

    printf( '<ul>%1$s</ul>', wp_list_categories( $args ) );

}