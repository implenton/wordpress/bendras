<?php

namespace Bendras\ID;

function get_template( $name, $post_type = 'page', $dir = 'templates' ) {

    if ( ! empty( $dir ) ) {
        $dir = trailingslashit( $dir );
    }

    $post = new \WP_Query( [
        'post_type'              => $post_type,
        'meta_key'               => '_wp_page_template',
        'posts_per_page'         => 1,
        'meta_value'             => "{$dir}{$name}.php",
        'no_found_rows'          => true,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    ] );

    if ( ! $post->posts ) {
        return false;
    }

    return $post->posts[0]->ID;

}