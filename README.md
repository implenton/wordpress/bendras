# Bendras

Develop WP sites…

1. Beware
2. General overview
    - Modules
    - Add support for modules
    - Versions
    - Add support for all modules and their latest version
3. Modules
    - acf
    - acf-table-field-renderer
    - cleanup
    - gf
    - guard-plugins
    - link
    - navigation
    - part
    - redirect
    - setup-theme
    - svg
    - typekit
    - wrap
4. Recommended plugins

## Beware

This plugin is:

- very opinionated
- meant to be used with the latest version of WordPress and PHP
- currently an experiment
- not suitable for production websites
- in constant iteration

## General overview

…Is a collection of functions and classes…

### Modules

…We call a module a class or a set of functions which solve a certain problem or circle around one specific topic…

For example the _svg_ module helps with embedding SVG files. The _link_ module provides new function for retrieving URLs for the blog, front page, certain pages witch specific templates, etc. 

### Add support for modules

Once the plugin is installed and activated you have to add support for the modules you want to use.

```$php
add_action( 'after_setup_theme', 'bendras_theme_support' );

function bendras_theme_support() {

    add_theme_support( 'bendras', [
        'code-name:version',
        // …
    ] );
    
}
```

Typically you will have this code in your `functions.php` file.

### Versions

To prevent breaking changes every module is code named.

Updating the plugin in itself won't cause any conflict because you have to declare the module version.

An update might contain new module or new versions of modules, but it is not loaded if it is not supported.

### Add support for all modules and their latest version

```$php
add_action( 'after_setup_theme', 'bendras_theme_support' );

function bendras_theme_support() {

    add_theme_support( 'bendras', [
        'acf:areng',
        'acf-table-field-renderer:areng',
        'cleanup:areng',
        'gf:areng',
        'guard-plugins:areng',
        'link:areng',
        'navigation:areng',
        'part:areng',
        'redirect:areng',
        'setup-theme:areng',
        'svg:areng',
        'wrap:areng',
    ] );
    
}
```

## Modules

### acf

*Versions:*

- areng

### acf-table-field-renderer

*Versions:*

- areng

### cleanup

*Versions:*

- areng

### gf

*Versions:*

- areng

### guard-plugins

*Versions:*

- areng

Removes the deactivate end edit links from the installed plugins section for certain plugins.

Helpful if you rely heavily on certain plugins and you want to prevent clients to deactivate plugins which would break functionality.

You can use the `bendras_guarded_plugins` filter to modify the default list of guarded plugins.

*Versions:*

- areng

### link

A set of helper function to link to different sections of your website.

*Versions:*

- areng

### navigation

*Versions:*

- areng

### part

*Versions:*

- areng

### redirect

*Versions:*

- areng

### setup-theme

*Versions:*

- areng

### svg

*Versions:*

- areng

### typekit

*Versions:*

- areng

### wrap

*Versions:*

- areng

## Recommended plugins

- Advanced Custom Fields PRO
- Advanced Custom Fields: Table Field
- GitHub Updater
- Gravity Forms
- Jaotama
- Safe SVG
- Tinklas
- WP Chosen
- WP Media Categories
- WP User Profiles
- Yoast SEO
- Duplicate Page F
- Duplicate Page F Extension