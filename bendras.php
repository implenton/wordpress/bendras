<?php
/*
 * Plugin Name:       Bendras
 * Plugin URI:        https://gitlab.com/implenton/wordpress/bendras
 * Description:       Develop WP sites faster
 * Version:           0.6.1
 * Text Domain:       bendras
 * Author:            implenton
 * Author URI:        https://implenton.com
 * GitLab Plugin URI: implenton/wordpress/bendras
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
*/

namespace Bendras;

add_action( 'after_setup_theme', __NAMESPACE__ . '\\load', 20 );

function load() {

    global $_wp_theme_features;

    if ( isset( $_wp_theme_features['bendras'] ) ) {

        require plugin_dir_path( __FILE__ ) . "inc/Bendras.php";
        new Bendras( $_wp_theme_features['bendras'] );

    }

}